---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Dorian
nom: Robin

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil


Dorian Robin 

![](https://favforward.com/app/uploads/2017/03/03.png)

Je m'appelle Dorian Robin, j'ai 19 ans et j'habite à Nantes. J'aime les jeux-vidéos, le football et les animés-japonais
## Contact:


e-mail: rdorian329@gmail.com Numéro: 0768331056

## Formation


*   2022-2023: Bac Technologique Lieu: _Châteaubriant_  
    
*   2023-2024: BTS SIO (Services Informatiques aux Organisations) Lieu: _Nantes_ 

Spécialité envisagée: SISR (Solution d'Infrastructure Système et Réseau)

## Compétences

    
|  Compétences |   Logiciel  |       
| ------------ | ----------- |
|   Programme  |    Python   |      
|   Système    |    Windows  |       
    
## Expérience

    
Été 2023: Job d'été FMGC _Soudan_ ; Secteur: Maintenance

3 Sous expériences:

* J'ai changé les composants de ma tour de PC avec ami et qui en même temps m'expliquait plein d'inforamtions sur les composants.

* En cours de système et réseau, j'ai enlevé et remis les composants d'une ancienne tour de PC.

* En cours de système et réseau, j'ai fait un TP de virtualisation de Windows XP. Ça m'avait beaucoup intéressé et j'était curieux de découvrir un des anciens systèmes d'exploitation Windows.
